using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using UnityEngine.Networking;

[InitializeOnLoad]
public class VersionChecker
{
    private static string m_remoteVer;
    private static string m_adress = "https://pastebin.com/raw/M2JBDPMq";

    private static int[] currentVerInt;
    private static int[] remoteVerInt;

    static VersionChecker()
    {
        EditorApplication.update += VersionCehck;
    }

    private static void VersionCehck()
    {
        if (!SessionState.GetBool("versionChecked", false))
        {
            GetLatestVersion();
        }
        EditorApplication.update -= VersionCehck;
    }

    static async void GetLatestVersion()
    {
        using UnityWebRequest webRequest = UnityWebRequest.Get(m_adress);
        var operation = webRequest.SendWebRequest();
        while (!operation.isDone)
            await System.Threading.Tasks.Task.Yield();
        if (webRequest.result == UnityWebRequest.Result.Success)
        {
            m_remoteVer = webRequest.downloadHandler.text;
            CompareVersion();
        }
        else
        {
            Debug.LogWarning($"Failed to get SDK version: {webRequest.error}");
        }
        SessionState.SetBool("versionChecked", true);
    }

    private static void CompareVersion()
    {
        currentVerInt = ParseVersion(HoopslySettings.Instance.SdkVersion);
        remoteVerInt = ParseVersion(m_remoteVer);
        for (int i = 0; i < currentVerInt.Length; i++)
        {
            if(currentVerInt[i]<remoteVerInt[i])
            {
                Debug.LogWarning("YOUR VERSION IS OUTDATED!");
                VersionWarningWindow.Init(m_remoteVer);
                break;
            }    
        }
    }


    private static int[] ParseVersion(string ver)
    {
        List<string> verNumbers = ver.Split('.').ToList();
        string last = verNumbers[verNumbers.Count - 1];
        verNumbers.RemoveAt(verNumbers.Count - 1);
        verNumbers.AddRange(last.Split('f'));
        int[] verValues = new int[verNumbers.Count];
        for (int i = 0; i < verNumbers.Count; i++)
        {
            verValues[i] = System.Convert.ToInt32(verNumbers[i]);
        }
        return verValues;
    }
}
