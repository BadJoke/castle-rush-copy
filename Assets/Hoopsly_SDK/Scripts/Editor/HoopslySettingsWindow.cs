﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;
using AppLovinMax.Scripts.IntegrationManager.Editor;
using Facebook.Unity.Settings;

public class HoopslySettingsWindow : EditorWindow
{
    private GUIStyle titleLabelStyle;
    private GUIStyle TitleLableStyle
    {
        get
        {
            if(titleLabelStyle==null)
            {
                titleLabelStyle = new GUIStyle(EditorStyles.label)
                {
                    fontSize = 14,
                    fontStyle = FontStyle.Bold,
                    fixedHeight = 20
                };
            }
            return titleLabelStyle;
        }
    }
    private GUIStyle headerLabelStyle;
    private GUIStyle HeaderLableStyle
    {
        get
        {
            if(headerLabelStyle == null)
            {
                headerLabelStyle = new GUIStyle(EditorStyles.label)
                {
                    fontSize = 12,
                    fontStyle = FontStyle.Bold,
                    fixedHeight = 18
                };
            }
            return headerLabelStyle;
        }
    }

    private GUIStyle versionHeaderStyle;
    private GUIStyle VersionHeaderLableStyle
    {
        get
        {
            if (versionHeaderStyle == null)
            {
                versionHeaderStyle = new GUIStyle(EditorStyles.label)
                {
                    fontSize = 12,
                    fontStyle = FontStyle.Bold,
                    fixedHeight = 18,
                    alignment = TextAnchor.MiddleRight
                };
            }
            return versionHeaderStyle;
        }
    }

    private FacebookSettings m_facebookSettings;
    private FacebookSettings Facebook_Settings
    {
        get
        {
            if(m_facebookSettings==null)
            {
                m_facebookSettings = (FacebookSettings)Resources.Load("FacebookSettings");
            }
            return m_facebookSettings;
        }
    }

    private string m_facebookAppName;
    private string m_facebookAppId;
    private string m_facebookClientToken;
    private string m_facebookAndroidKeystore;
    private string m_facebookIosURLSuffix;

    [MenuItem("Hoopsly/Settings")]
    static void Init()
    {
        HoopslySettingsWindow window = (HoopslySettingsWindow)EditorWindow.GetWindow(typeof(HoopslySettingsWindow),false, "Hoopsly settings window");
        window.Show();
    }

    private void OnDestroy()
    {
        SaveSettingsAsset();
    }


    private void Awake()
    {
        m_facebookAppId = Facebook.Unity.Settings.FacebookSettings.AppId;
        m_facebookClientToken = Facebook.Unity.Settings.FacebookSettings.ClientToken;
        m_facebookAppName = Facebook.Unity.Settings.FacebookSettings.AppLabels[0];
        m_facebookAndroidKeystore = Facebook.Unity.Settings.FacebookSettings.AndroidKeystorePath;
        m_facebookIosURLSuffix = Facebook.Unity.Settings.FacebookSettings.IosURLSuffix;
    }
    Vector2 scrollPos;
    void OnGUI()
    {
        if (HoopslySettings.Instance != null)
        {
            scrollPos = GUILayout.BeginScrollView(scrollPos);
            GUILayout.Space(10);
            DrawBaseSettings();
            //GUILayout.Space(10);
            //DrawFirebaseSettings();
            GUILayout.Space(10);
            DrawAppsFlyerSettings();
            GUILayout.Space(10);
            DrawApplovinSettings();
            GUILayout.Space(10);
            DrawFacebookSettings();

            if (GUI.changed)
            {
                SaveSettingsAsset();
            }
            GUILayout.EndScrollView();
        }
    }

    private void DrawBaseSettings()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Hoopsly settings", TitleLableStyle);
        GUILayout.Label(HoopslySettings.Instance.SdkVersion, VersionHeaderLableStyle);
        GUILayout.EndHorizontal();
        using (var v = new EditorGUILayout.VerticalScope("box"))
        {
            GUILayout.Space(15);
            if (GUILayout.Button("Spawn integration prefab in current scene"))
            {
                SpawnIntegraionsPrefab();
            }
            GUILayout.Space(5);
            HoopslySettings.Instance.FPSmeasueIntervals = EditorGUILayout.Slider("FPS measurment intervals (in sec)", HoopslySettings.Instance.FPSmeasueIntervals, .1f, 2f );
            GUILayout.Space(5);
            HoopslySettings.Instance.DebugFpsMeasurment = EditorGUILayout.ToggleLeft("Enable fps measure logs", HoopslySettings.Instance.DebugFpsMeasurment);
            GUILayout.Space(15);
        }
    }

    private void DrawFirebaseSettings()
    {
        GUILayout.Label("Firebase settings", TitleLableStyle);
        using (var v = new EditorGUILayout.VerticalScope("box"))
        {
            GUILayout.Space(15);
            HoopslySettings.Instance.UseFirebase = EditorGUILayout.ToggleLeft("Enable Firebase SDK", HoopslySettings.Instance.UseFirebase);
            GUILayout.Space(15);
        }
    }

    private void DrawAppsFlyerSettings()
    {
        GUILayout.Label("Appsflyer settings", TitleLableStyle);
        using (var v = new EditorGUILayout.VerticalScope("box"))
        {
            GUILayout.Space(15);
            HoopslySettings.Instance.UseAppsflyer = EditorGUILayout.ToggleLeft("Enable Appsflyer SDK", HoopslySettings.Instance.UseAppsflyer);
            if (HoopslySettings.Instance.UseAppsflyer)
            {
                GUILayout.Space(15);
                HoopslySettings.Instance.AppsFlyerSdkKey = EditorGUILayout.TextField("Appsflyer SDK key", HoopslySettings.Instance.AppsFlyerSdkKey);
                GUILayout.Space(5);
                HoopslySettings.Instance.AppsFlyerAppID = EditorGUILayout.TextField("AppsFlyer App ID", HoopslySettings.Instance.AppsFlyerAppID);
                GUILayout.Space(5);
                HoopslySettings.Instance.AppsflyerIsDebug = EditorGUILayout.Toggle("Is Debug", HoopslySettings.Instance.AppsflyerIsDebug);
            }
            GUILayout.Space(15);
        }
    }

    bool showInter = false;
    bool showReward = false;
    bool showBanner = false;
    bool showMREC = false;

    private void DrawApplovinSettings()
    {
        GUILayout.Label("Applovin MAX settings", TitleLableStyle);
        using (var v = new EditorGUILayout.VerticalScope("box"))
        {
            GUILayout.Space(15);
            HoopslySettings.Instance.UseApplovin = EditorGUILayout.ToggleLeft("Enable Applovin SDK", HoopslySettings.Instance.UseApplovin);

            if (HoopslySettings.Instance.UseApplovin)
            {
                GUILayout.Space(15);
                HoopslySettings.Instance.MaxSdkKey = EditorGUILayout.TextField("Applovin MAX SDK key", HoopslySettings.Instance.MaxSdkKey);
                AppLovinSettings.Instance.SdkKey = HoopslySettings.Instance.MaxSdkKey;
                GUILayout.Space(5);

                HoopslySettings.Instance.EnableVerboseLogging = EditorGUILayout.ToggleLeft("Enable Verbose Logging", HoopslySettings.Instance.EnableVerboseLogging);
                GUILayout.Space(5);
                HoopslySettings.Instance.ShowMediationDebuggerOnLoad = EditorGUILayout.ToggleLeft(new GUIContent("Show Mediation debugger on app start"), HoopslySettings.Instance.ShowMediationDebuggerOnLoad);
                
                using (var adSettings = new EditorGUILayout.VerticalScope("box"))
                {
                    DrawInterstitialSettings();

                    DrawRewardedSettings();

                    DrawMRECsettings();

                    DrawBannerSettings();
                }

                GUILayout.Space(20);

                if (GUILayout.Button("Open Applovin integration manager"))
                {
                    AppLovinIntegrationManagerWindow.ShowManager();
                }
            }
            GUILayout.Space(15);
        }
    }

    private void DrawInterstitialSettings()
    {
        showInter = EditorGUILayout.BeginFoldoutHeaderGroup(showInter, "Interstitial AD");
        if (showInter)
        {
            using (var inter = new EditorGUILayout.VerticalScope("box"))
            {
                GUILayout.Space(5);
                HoopslySettings.Instance.UseInterstitialAd = EditorGUILayout.BeginToggleGroup("Enable interstitial AD", HoopslySettings.Instance.UseInterstitialAd);
                HoopslySettings.Instance.InterstitialAdUnitID = EditorGUILayout.TextField("Interstitial AD Unit ID", HoopslySettings.Instance.InterstitialAdUnitID);
                EditorGUILayout.EndToggleGroup();
                GUILayout.Space(5);
            }
        }
        EditorGUILayout.EndFoldoutHeaderGroup();
    }

    private void DrawRewardedSettings()
    {
        GUILayout.Space(10);
        showReward = EditorGUILayout.BeginFoldoutHeaderGroup(showReward, "Reward AD");
        if (showReward)
        {
            GUILayout.Space(15);
            HoopslySettings.Instance.UseRewardedAd = EditorGUILayout.BeginToggleGroup("Enable Rewarded AD", HoopslySettings.Instance.UseRewardedAd);
            HoopslySettings.Instance.RewardedAdUnitID = EditorGUILayout.TextField("Rewarded AD Unit ID", HoopslySettings.Instance.RewardedAdUnitID);
            EditorGUILayout.EndToggleGroup();
            GUILayout.Space(15);
        }
        EditorGUILayout.EndFoldoutHeaderGroup();
    }

    private void DrawMRECsettings()
    {
        GUILayout.Space(10);
        showMREC = EditorGUILayout.BeginFoldoutHeaderGroup(showMREC, "MREC AD");
        if (showMREC)
        {
            GUILayout.Space(15);
            HoopslySettings.Instance.UseMRECAd = EditorGUILayout.BeginToggleGroup("Enable MREC AD", HoopslySettings.Instance.UseMRECAd);
            HoopslySettings.Instance.MRECAdUnitID = EditorGUILayout.TextField("MREC AD Unit ID", HoopslySettings.Instance.MRECAdUnitID);
            GUILayout.Space(5);
            HoopslySettings.Instance.MrecPosition = (MaxSdkBase.AdViewPosition)EditorGUILayout.EnumPopup("Select MREC position", HoopslySettings.Instance.MrecPosition);
            EditorGUILayout.EndToggleGroup();
            GUILayout.Space(15);
        }
        EditorGUILayout.EndFoldoutHeaderGroup();
    }

    private void DrawBannerSettings()
    {
        GUILayout.Space(10);
        showBanner = EditorGUILayout.BeginFoldoutHeaderGroup(showBanner, "Banner AD");
        if (showBanner)
        {
            GUILayout.Space(5);
            HoopslySettings.Instance.UseBannerAd = EditorGUILayout.BeginToggleGroup("Enable Banner AD", HoopslySettings.Instance.UseBannerAd);
            HoopslySettings.Instance.BannerAdUnitID = EditorGUILayout.TextField("Banner AD Unit ID", HoopslySettings.Instance.BannerAdUnitID);
            GUILayout.Space(5);
            HoopslySettings.Instance.UseAdaptiveBanner = EditorGUILayout.ToggleLeft("Use adaptive banner", HoopslySettings.Instance.UseAdaptiveBanner);
            GUILayout.Space(5);
            HoopslySettings.Instance.BannerPosition = (MaxSdk.BannerPosition)EditorGUILayout.EnumPopup("Select banner position", HoopslySettings.Instance.BannerPosition);
            GUILayout.Space(5);
            HoopslySettings.Instance.BannerBackgroundColor = EditorGUILayout.ColorField("Background color", HoopslySettings.Instance.BannerBackgroundColor);
            EditorGUILayout.EndToggleGroup();
            GUILayout.Space(5);
        }
        EditorGUILayout.EndFoldoutHeaderGroup();
    }

    private bool m_facebookAndroidSettings = false;
    private bool m_facebookIOSsettinghs = false;
    private void DrawFacebookSettings()
    {
        GUILayout.Label("Facebook settings", TitleLableStyle);
        using (var v = new EditorGUILayout.VerticalScope("box"))
        {
            GUILayout.Space(15);
            HoopslySettings.Instance.UseFacebook = EditorGUILayout.ToggleLeft("Enable Facebook SDK", HoopslySettings.Instance.UseFacebook);
            if(HoopslySettings.Instance.UseFacebook)
            {
                EditorGUI.BeginChangeCheck();
                m_facebookAppName = EditorGUILayout.DelayedTextField("App name (optiona)", m_facebookAppName);
                if (EditorGUI.EndChangeCheck())
                {
                    Facebook.Unity.Settings.FacebookSettings.AppLabels = new List<string>() { m_facebookAppName };
                    EditorUtility.SetDirty(Facebook_Settings);
                }
                GUILayout.Space(5);
                EditorGUI.BeginChangeCheck();
                m_facebookAppId = EditorGUILayout.DelayedTextField("App ID", m_facebookAppId);
                if(EditorGUI.EndChangeCheck())
                {
                    Facebook.Unity.Settings.FacebookSettings.AppIds = new List<string>() { m_facebookAppId };
                    EditorUtility.SetDirty(Facebook_Settings);
                }
                GUILayout.Space(5);
                EditorGUI.BeginChangeCheck();
                m_facebookClientToken = EditorGUILayout.DelayedTextField("Client Token (Optional)", m_facebookClientToken);
                if (EditorGUI.EndChangeCheck())
                {
                    Facebook.Unity.Settings.FacebookSettings.ClientTokens = new List<string>() { m_facebookClientToken };
                    EditorUtility.SetDirty(Facebook_Settings);
                }

                GUILayout.Space(5);
                m_facebookAndroidSettings = EditorGUILayout.BeginFoldoutHeaderGroup(m_facebookAndroidSettings, "Android Settings");
                if(m_facebookAndroidSettings)
                {
                    EditorGUI.BeginChangeCheck();
                    m_facebookAndroidKeystore = EditorGUILayout.DelayedTextField("Android keystore path", m_facebookAndroidKeystore);
                    if (EditorGUI.EndChangeCheck())
                    {
                        Facebook.Unity.Settings.FacebookSettings.AndroidKeystorePath = m_facebookAndroidKeystore;
                        EditorUtility.SetDirty(Facebook_Settings);
                    }
                }
                EditorGUILayout.EndFoldoutHeaderGroup();
                GUILayout.Space(5);
                m_facebookIOSsettinghs = EditorGUILayout.BeginFoldoutHeaderGroup(m_facebookIOSsettinghs, "iOS Settings");
                if(m_facebookIOSsettinghs)
                {
                    EditorGUI.BeginChangeCheck();
                    m_facebookIosURLSuffix = EditorGUILayout.DelayedTextField("iOS URL Scheme Suffix", m_facebookIosURLSuffix);
                    if (EditorGUI.EndChangeCheck())
                    {
                        Facebook.Unity.Settings.FacebookSettings.IosURLSuffix = m_facebookIosURLSuffix;
                        EditorUtility.SetDirty(Facebook_Settings);
                    }
                }
                EditorGUILayout.EndFoldoutHeaderGroup();
                GUILayout.Space(5);

                if (GUILayout.Button("Regenerate android manifest"))
                {
                    Facebook.Unity.Editor.ManifestMod.GenerateManifest();
                    EditorUtility.SetDirty(Facebook_Settings);
                }

                GUILayout.Space(20);

                if (GUILayout.Button("Open facebook settings"))
                {
                    FacebookSettings facebookSettings = Resources.Load("FacebookSettings") as FacebookSettings;
                    EditorGUIUtility.PingObject(facebookSettings);
                    Selection.activeObject = facebookSettings;
                }

            }
            GUILayout.Space(15);
        }
    }

    private void SaveSettingsAsset()
    {
        HoopslySettings.Instance.SaveSettingsAsync();
    }

    private void SpawnIntegraionsPrefab()
    {
        HoopslyIntegration integrations = FindObjectOfType<HoopslyIntegration>();
        if (integrations == null)
        {
            GameObject instance = Resources.Load("HoopslySDK_Integrations", typeof(GameObject)) as GameObject;
            var prefab =PrefabUtility.InstantiatePrefab(instance);
            EditorUtility.SetDirty(prefab);
        }
        else
        {
            Debug.Log("Integrations pprfab already in scene");
        }
    }
}
