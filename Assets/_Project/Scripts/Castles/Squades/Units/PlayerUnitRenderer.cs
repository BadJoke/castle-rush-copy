﻿using DG.Tweening;
using System;
using UnityEngine;

namespace Castles.Squades.Units
{
    public class PlayerUnitRenderer : MonoBehaviour
    {
        [SerializeField] private float _size = 0.25f;
        [SerializeField] private Transform _puff;
        [SerializeField] private Blot _blot;
        [SerializeField] private Renderer _renderer;

        private bool _isMoving;
        private Sequence _activeSequence;

        public event Action Hided;

        private void OnEnable()
        {
            Show();
        }

        public void AttackCastle()
        {
            transform.SetParent(null);

            _activeSequence = DOTween.Sequence()
                .Append(_puff.DOScale(4f, 0.05f))
                .Append(transform.DOScale(0f, 0.2f));
            _activeSequence.onComplete += () =>
            {
                if (_isMoving)
                {
                    StopHiding();
                }
            };

        }

        public void Hide()
        {
            transform.SetParent(null);

            _activeSequence = DOTween.Sequence()
                .Append(transform.DOScale(0f, 0.25f));
            _activeSequence.onComplete += () =>
            {
                if (_isMoving)
                {
                    StopHiding();
                }
            };
        }

        public void Blot()
        {
            transform.SetParent(null);
            _blot.Show();
            Hided?.Invoke();
        }

        private void Show()
        {
            _isMoving = true;
            _puff.localScale = Vector3.zero;
            transform.localScale = Vector3.zero;
            _blot.Init(transform, _renderer.material.color);

            DOTween.Sequence()
                .Append(transform.DOScale(_size * 1.2f, 0.2f))
                .Append(transform.DOScale(_size * 1f, 0.05f));
        }

        private void StopHiding()
        {
            _isMoving = false;
            _activeSequence?.Kill();
            Hided?.Invoke();
        }
    }
}