﻿using UnityEngine;

namespace Castles.Squades.Units
{
    [RequireComponent(typeof(Animator))]
    public class FinalUnitAnimator : MonoBehaviour
    {
        private Animator _animator;

        private const string MovingBool = "IsMoving";
        private const string AttackTrigger = "Attack";

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        public void Stay()
        {
            _animator.SetBool(MovingBool, false);
        }

        public void Run()
        {
            _animator.SetBool(MovingBool, true);
        }

        public void Attack()
        {
            _animator.SetTrigger(AttackTrigger);
        }
    }
}