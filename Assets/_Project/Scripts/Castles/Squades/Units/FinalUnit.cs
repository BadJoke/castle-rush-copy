﻿using DG.Tweening;
using FUGames.Pooling;
using System;
using UnityEngine;

namespace Castles.Squades.Units
{
    [RequireComponent(typeof(FinalUnitRenderer), typeof(FinalUnitAnimator), typeof(FinalUnitMovement))]
    public class FinalUnit : PoolObject
    {
        [SerializeField] private float _moveToPositionTime = 0.5f;

        private FinalUnit _enemy;
        private FinalUnitAnimator _animator;
        private FinalUnitRenderer _renderer;
        private FinalUnitMovement _movement;

        public event Action<FinalUnit> Destroyed;
        public event Action<FinalUnit> EnemyDestroyed;

        private void Awake()
        {
            _animator = GetComponent<FinalUnitAnimator>();
            _renderer = GetComponent<FinalUnitRenderer>();
            _movement = GetComponent<FinalUnitMovement>();

            _movement.EnemyReached += OnEnemyReached;
            _movement.CastleReached += OnCastleReached;
        }

        private void OnDestroy()
        {
            _movement.EnemyReached -= OnEnemyReached;
            _movement.CastleReached -= OnCastleReached;
        }

        public void Init(Transform parent, Vector3 startPosition, Vector3 squadePosition, Material skin)
        {
            _animator.Run();
            _renderer.Init(skin);

            gameObject.SetActive(true);

            transform.SetParent(parent);
            transform.position = startPosition;
            transform.localRotation = Quaternion.LookRotation(squadePosition.normalized);
            transform.DOLocalMove(squadePosition, _moveToPositionTime)
                .onComplete += () =>
                {
                    transform.rotation = parent.rotation;
                    _animator.Stay();
                };
        }

        public void MoveToEnemy(FinalUnit enemy)
        {
            _enemy = enemy;
            enemy.Destroyed += OnEnemyDesroyed;

            MoveToTarget(true, enemy.transform, enemy.transform);
        }

        public void MoveToCastle(Transform target, Transform castle)
        {
            MoveToTarget(false, target,castle);
        }

        public void Destroy()
        {
            _renderer.ShowBlot();

            Die();
        }

        private void MoveToTarget(bool isEnemy, Transform target, Transform checkingtarget)
        {
            _movement.MoveToTarget(isEnemy, target, checkingtarget);
            _animator.Run();
        }

        private void Die()
        {
            Destroyed?.Invoke(this);
            BackToPool();
        }

        private void AttackEnemy()
        {
            _enemy.Destroy();
        }

        private void OnEnemyDesroyed(FinalUnit enemy)
        {
            enemy.Destroyed -= OnEnemyDesroyed;

            EnemyDestroyed?.Invoke(this);
        }

        private void OnEnemyReached()
        {
            _animator.Attack();
        }

        private void OnCastleReached()
        {
            Die();
        }
    }

    public class GoodFinalUnit : FinalUnit { }
    public class BadFinalUnit : FinalUnit { }
}