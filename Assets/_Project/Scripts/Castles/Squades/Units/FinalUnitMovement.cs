﻿using System;
using UnityEngine;

namespace Castles.Squades.Units
{
    public class FinalUnitMovement : MonoBehaviour
    {
        [SerializeField] private float _speed = 5f;
        [SerializeField] private float _toCastleMovingModifier = 1.5f;

        private bool _isMoveToEnemy;
        private float _targetOffset;
        private Transform _followingTarget;
        private Transform _checkingTarget;

        private const float EnemyReachingOffset = 0.1f;
        private const float CastleReachingOffset = 2.5f;

        public event Action EnemyReached;
        public event Action CastleReached;

        private void Update()
        {
            var direction = (_followingTarget.position - transform.position).normalized;
            var moving = direction * _speed * Time.deltaTime;

            if (_isMoveToEnemy == false)
            {
                moving *= _toCastleMovingModifier;
            }

            transform.rotation = Quaternion.LookRotation(direction);
            transform.Translate(moving, Space.World);

            if (Vector3.Distance(transform.position, _checkingTarget.position) < _targetOffset)
            {
                if (_isMoveToEnemy)
                {
                    enabled = false;
                    EnemyReached?.Invoke();
                }
                else
                {
                    CastleReached?.Invoke();
                }
            }
        }

        public void MoveToTarget(bool isEnemy, Transform target, Transform checkingtarget)
        {
            _isMoveToEnemy = isEnemy;
            _targetOffset = isEnemy ? EnemyReachingOffset : CastleReachingOffset;
            _followingTarget = target;
            _checkingTarget = checkingtarget;

            transform.LookAt(target);
            enabled = true;
        }
    }
}