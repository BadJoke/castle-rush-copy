﻿using DG.Tweening;
using FUGames.Pooling;
using UnityEngine;

namespace Castles.Squades.Units
{
    [RequireComponent(typeof(BoxCollider), typeof(Rigidbody))]
    public class EnemyUnit : PoolObject
    {
        [SerializeField] private Blot _blot;
        [SerializeField] private Renderer _renderer;

        private Collider _collider;

        private void Awake()
        {
            _collider = GetComponent<Collider>();   
        }

        public void Init(Transform parent, Vector3 position, float sign)
        {
            transform.SetParent(parent);
            transform.localPosition = position;

            var direction = (parent.position - transform.position).normalized;
            transform.rotation = Quaternion.LookRotation(direction);
            transform.Rotate(Vector3.up * -90f * sign);

            gameObject.SetActive(true);

            _blot.Init(transform, _renderer.material.color);
        }

        public void Die()
        {
            _collider.enabled = false;

            _blot.Show();

            BackToPool();
        }
    }
}