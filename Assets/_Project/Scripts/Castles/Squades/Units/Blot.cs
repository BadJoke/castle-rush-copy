﻿using DG.Tweening;
using UnityEngine;

namespace Castles.Squades.Units
{
    public class Blot : MonoBehaviour
    {
        [SerializeField] private float _blotSize = 1f;
        [SerializeField] private float _fadingDelay = 1f;
        [SerializeField] private float _fadingTime = 0.5f;
        [SerializeField] private SpriteRenderer _blot;
        [SerializeField] private ParticleSystem _effect;
        [SerializeField] private ParticleSystem _subEffect;

        public void Init(Transform parent, Color color)
        {
            var main = _subEffect.main;
            main.startColor = color;
            _subEffect.GetComponent<ParticleSystemRenderer>().material.color = color;
            _blot.color = color;

            transform.SetParent(parent);
            transform.localPosition = Vector3.zero;

            _blot.transform.localScale = Vector3.zero;
        }

        public void Show()
        {
            if (transform.parent == null)
            {
                return;
            }

            transform.SetParent(null);
            transform.localScale = Vector3.one * 0.5f;

            _effect.gameObject.SetActive(true);
            _effect.Play();
            _blot.transform.DOScale(_blotSize, 0.5f)
                .onComplete += () =>
                {
                    Invoke(nameof(DoFade), _fadingDelay);
                };
        }

        private void DoFade()
        {
            _blot.material.DOFade(0f, _fadingTime);
        }
    }
}