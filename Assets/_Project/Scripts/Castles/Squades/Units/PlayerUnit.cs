﻿using FUGames.Pooling;
using System;
using UnityEngine;

namespace Castles.Squades.Units
{
    [RequireComponent(typeof(PlayerUnitRenderer), typeof(BoxCollider))]
    public class PlayerUnit : PoolObject
    {
        [SerializeField] private float _radiusModifier = 1.3f;

        private bool _isDied = false;
        private float _deathRadius;
        private Vector3 _targetPosition;
        private PlayerUnitRenderer _rendering;

        private const int _enemyLayer = 10;

        public event Action<PlayerUnit> Died;
        public event Action<PlayerUnit> TargetReached;

        private void Awake()
        {
            _rendering = GetComponent<PlayerUnitRenderer>();
            _rendering.Hided += OnHided;
        }

        private void OnTriggerEnter(Collider collider)
        {
            var colided = collider.gameObject;

            if (colided.layer == _enemyLayer)
            {
                if (colided.TryGetComponent(out EnemyUnit unit))
                {
                    unit.Die();
                }

                _isDied = true;
                _rendering.Blot();
            }
        }

        private void OnDestroy()
        {
            _rendering.Hided -= OnHided;
        }

        public void Init(Vector3 targetPosition, float radius, Transform parent, Vector3 localPosition)
        {
            _isDied = false;
            _targetPosition = targetPosition;
            _deathRadius = radius * _radiusModifier;

            transform.SetParent(parent);
            transform.localPosition = localPosition;
            transform.rotation = parent.rotation;

            enabled = true;
            gameObject.SetActive(true);
        }

        public void CheckDistance()
        {
            if (Vector3.Distance(_targetPosition, transform.position) < _deathRadius)
            {
                enabled = false;

                _rendering.AttackCastle();
            }
        }

        public void Hide()
        {
            _rendering.Hide();
        }

        private void OnHided()
        {
            if (_isDied)
            {
                Died?.Invoke(this);
            }
            else
            {
                TargetReached?.Invoke(this);
            }
            BackToPool();
        }
    }
}