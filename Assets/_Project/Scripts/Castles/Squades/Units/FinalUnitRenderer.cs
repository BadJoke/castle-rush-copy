﻿using UnityEngine;

namespace Castles.Squades.Units
{
    public class FinalUnitRenderer : MonoBehaviour
    {
        [SerializeField] private SkinnedMeshRenderer _skin;
        [SerializeField] private Blot _blot;

        public void Init(Material skin)
        {
            _skin.material = skin;
            _blot.Init(transform, skin.color);
        }

        public void ShowBlot()
        {
            _blot.Show();
        }
    }
}