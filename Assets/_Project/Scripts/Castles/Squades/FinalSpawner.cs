﻿using Castles.Squades.Units;
using FUGames.Pooling;
using System;
using System.Collections;
using UnityEngine;

namespace Castles.Squades
{
    [Serializable]
    public class FinalSpawner
    {
        [SerializeField] private int _unitCost = 3;
        [SerializeField] private int _lineWidth = 20;
        [SerializeField] private float _offset = 1f;
        [SerializeField] private float _spawnDelay;
        [SerializeField] private Vector3 _spawnPoint = new Vector3(0f, 0f, 10f);
        [SerializeField] private float _unitSpawningDelay = 0.01f;
        [SerializeField] private float _lineSpawningDelay = 0.05f;
        [SerializeField] private Material _unitSkin;
        [SerializeField] private MonoBehaviour _owner;
        [SerializeField] private PoolManager _poolManager;

        private int _count;

        public event Action<FinalUnit> UnitSpawned;
        public event Action Spawned;

        public float GetTime()
        {
            var lines = _count / _lineWidth;
            return lines * _lineSpawningDelay + _count * _unitSpawningDelay + 1.5f;
        }

        public void Spawn(int power, Transform parent, Vector3 startPosition)
        {
            _owner.StartCoroutine(SpawnUnits(power, parent, startPosition));
        }

        private IEnumerator SpawnUnits(int power, Transform parent, Vector3 startPosition)
        {
            _count = CalculateUnitCount(power);
            var lastLineWidth = CalculateLastLineWidth(_count);

            yield return new WaitForSeconds(_spawnDelay);

            while (_count > 0)
            {
                _count -= _lineWidth;

                var width = _count > 0 ? _lineWidth : lastLineWidth;
                var offset = Vector3.right * _offset;
                var start = -width / 2f + 0.5f;
                var end = width / 2f + 0.5f;

                for (float i = start; i < end; i++)
                {
                    var squadePosition = _spawnPoint + offset * i;

                    Spawn(parent, startPosition, squadePosition);

                    yield return new WaitForSeconds(_unitSpawningDelay);
                }

                yield return new WaitForSeconds(_lineSpawningDelay);

                _spawnPoint += Vector3.back * _offset;
            }

            Spawned?.Invoke();
        }

        private int CalculateLastLineWidth(int count)
        {
            var lastLineWidth = count % _lineWidth;

            if (lastLineWidth == 0)
            {
                lastLineWidth = _lineWidth;
            }

            return lastLineWidth;
        }

        private int CalculateUnitCount(int power)
        {
            var count = power / _unitCost;

            if (power % _unitCost != 0)
            {
                count++;
            }

            return count;
        }

        private void Spawn(Transform parent, Vector3 startPosition, Vector3 squadePosition)
        {
            var unit = _poolManager.Take<FinalUnit>();
            unit.Init(parent, startPosition, squadePosition, _unitSkin);
            UnitSpawned?.Invoke(unit);
        }
    }
}