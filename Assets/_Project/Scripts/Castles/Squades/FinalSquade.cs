﻿using Castles.Squades.Units;
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Castles.Squades
{
    public class FinalSquade : MonoBehaviour
    {
        [SerializeField] private float _flowSpeed;
        [SerializeField] private Transform _castleFlow;
        [SerializeField] private FinalSpawner _spawner;

        private FinalSquade _enemy;
        private List<FinalUnit> _units = new List<FinalUnit>();

        public event Action Destroyed;

        public FinalSpawner Spawner => _spawner;
        public List<FinalUnit> Units => _units;

        private void Awake()
        {
            enabled = false;
            _spawner.UnitSpawned += OnUnitSpawned;
        }

        private void Update()
        {
            _castleFlow.Translate(Vector3.forward * _flowSpeed * Time.deltaTime);
        }

        private void OnDestroy()
        {
            _spawner.UnitSpawned -= OnUnitSpawned;
        }

        public void Init(int power, Vector3 startPosition)
        {
            _spawner.Spawn(power, transform, startPosition);
        }

        public void StartMovement(FinalSquade enemy)
        {
            _enemy = enemy;

            var targets = enemy.Units;

            for (int i = 0; i < _units.Count; i++)
            {
                _units[i].MoveToEnemy(targets[Random.Range(0, targets.Count)]);
            }
        }

        public void MoveToCastle(Transform castle)
        {
            enabled = true;

            var unitIndex = 0;
            var offset = 1.5f;
            var lineWidth = 3;
            var count = _units.Count;
            var lastLineWidth = count % lineWidth;
            var spawnPoint = Vector3.zero;

            if (lastLineWidth == 0)
            {
                lastLineWidth = lineWidth;
            }

            while (count > 0)
            {
                count -= lineWidth;

                var width = count > 0 ? lineWidth : lastLineWidth;
                var currentOffset = Vector3.right * offset;
                var start = -width / 2f + 0.5f;
                var end = width / 2f + 0.5f;

                for (float i = start; i < end; i++)
                {
                    var position = spawnPoint + currentOffset * i;

                    var point = new GameObject().transform;
                    point.SetParent(_castleFlow);
                    point.localPosition = position;

                    _units[unitIndex++].MoveToCastle(point, castle);
                }

                spawnPoint.z -= offset * 0.15f;
            }
        }

        private void OnUnitSpawned(FinalUnit unit)
        {
            unit.Destroyed += OnUnitDestroyed;
            unit.EnemyDestroyed += OnUnitDestroyEnemy;
            _units.Add(unit);
        }

        private void OnUnitDestroyEnemy(FinalUnit unit)
        {
            if (_enemy.Units.Count == 0)
            {
                return;
            }

            unit.MoveToEnemy(_enemy.Units[Random.Range(0, _enemy.Units.Count)]);
        }

        private void OnUnitDestroyed(FinalUnit unit)
        {
            unit.Destroyed -= OnUnitDestroyed;

            _units.Remove(unit);

            if (_units.Count == 0)
            {
                enabled = false;
                Destroyed?.Invoke();
            }
        }
    }
}