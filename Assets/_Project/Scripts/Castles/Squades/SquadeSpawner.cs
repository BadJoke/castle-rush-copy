﻿using Castles.Squades.Units;
using FUGames.Pooling;
using System;
using System.Collections;
using UnityEngine;

namespace Castles.Squades
{
    [Serializable]
    public class SquadeSpawner
    {
        [SerializeField] private float _delay;
        [SerializeField] private float _radius;
        [SerializeField] private float _offset;
        [SerializeField] private int _unitCost = 3;
        [SerializeField] private int _maxCountFor1Width = 8;
        [SerializeField] private int _maxCountFor2Width = 21;
        [SerializeField] private PlayerSquade _owner;
        
        private Vector3 _spawnPoint;
        private Coroutine _spawning;
        private Vector3 _targetPosition;
        private PoolManager _poolManager;

        public event Action<PlayerUnit> UnitSpawned;
        
        public void Spawn(Vector3 targetPosition, int amount)
        {
            _targetPosition = targetPosition;
            _spawnPoint = Vector3.zero;
            _poolManager = PoolManager.Instance;

            int count = CalculateUnitCount(amount);

            if (_spawning != null)
            {
                _owner.StopCoroutine(_spawning);
            }

            _spawning = _owner.StartCoroutine(RectSpawning(count));
        }

        private int CalculateUnitCount(int amount)
        {
            int count = amount / _unitCost;

            if (amount % _unitCost > 0)
            {
                count++;
            }

            return count;
        }

        private IEnumerator RectSpawning(int count)
        {
            var lineWidth = DetermineLineWidth(count);
            var lastLineWidth = count % lineWidth;

            if (lastLineWidth == 0)
            {
                lastLineWidth = lineWidth;
            }

            while (count > 0)
            {
                count -= lineWidth;

                SpawnLine(count > 0 ? lineWidth : lastLineWidth);

                yield return new WaitForSeconds(_delay);
            }
        }

        private int DetermineLineWidth(int count)
        {
            if (count < _maxCountFor1Width)
            {
                return 1;
            }
            else if (count < _maxCountFor2Width)
            {
                return 2;
            }

            return 3;
        }

        private void SpawnLine(int width)
        {
            var offset = Vector3.right * _offset;
            var start = -width / 2f + 0.5f;
            var end = width / 2f + 0.5f;

            for (float i = start; i < end; i++)
            {
                var position = _spawnPoint + offset * i;

                Spawn(position);
            }

            _spawnPoint += Vector3.back * _offset;
        }

        private void Spawn(Vector3 position)
        {
            var unit = _poolManager.Take<PlayerUnit>();
            unit.Init(_targetPosition, _radius, _owner.transform, position);

            UnitSpawned?.Invoke(unit);
        }
    }
}