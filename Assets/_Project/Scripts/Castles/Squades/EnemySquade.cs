﻿using Castles.Squades.Units;
using FUGames.Pooling;
using System.Collections.Generic;
using UnityEngine;

namespace Castles.Squades
{
    public class EnemySquade : MonoBehaviour
    {
        [SerializeField] private int _subSquadesCount;
        [SerializeField] private int _subSquadesSize;
        [SerializeField] private float _rotationSpeed;
        [SerializeField] private float _radius = 2.5f;
        [SerializeField] private float _unitOffset = 3f;
        [SerializeField] private Tower _owner;

        private float _offset;
        private PoolManager _poolManager;
        private List<EnemyUnit> _units = new List<EnemyUnit>();

        private void Start()
        {
            _offset = _unitOffset * Mathf.Deg2Rad;
            _poolManager = PoolManager.Instance;
            _owner.UnitsTaked += OnOwnerUnitsTaked;

            if (_subSquadesCount == 0 || _subSquadesSize == 0)
            {
                enabled = false;
            }
            else
            {
                SpawnSquade();
            }
        }

        private void Update()
        {
            transform.Rotate(Vector3.up * _rotationSpeed * Time.deltaTime, Space.World);
        }

        private void SpawnSquade()
        {
            var angleStep = 2f * Mathf.PI / _subSquadesCount;
            var startAngle = Random.Range(0f, 360f);

            for (int i = 0; i < _subSquadesCount; i++, startAngle += angleStep)
            {
                for (int j = 0; j < _subSquadesSize; j++)
                {
                    var angle = startAngle + (j * _offset);
                    var x = Mathf.Cos(angle) * _radius;
                    var z = Mathf.Sin(angle) * _radius;
                    var position = new Vector3(x, 0f, z);

                    var unit = _poolManager.Take<EnemyUnit>();
                    unit.Init(transform, position, Mathf.Sign(_rotationSpeed));
                    _units.Add(unit);
                }
            }
        }

        private void OnOwnerUnitsTaked(int count)
        {
            _owner.UnitsTaked -= OnOwnerUnitsTaked;

            enabled = false;

            foreach (var unit in _units)
            {
                unit.Die();
            }
        }
    }
}