﻿using Castles.Squades.Units;
using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Castles.Squades
{
    [RequireComponent(typeof(BoxCollider))]
    public class PlayerSquade : MonoBehaviour
    {
        [SerializeField] private float _speed = 12f;
        [SerializeField] private SquadeSpawner _spawner;
        [SerializeField] private Text _display;

        private bool _isMoving;
        private int _power;
        private Tower _target;
        private List<PlayerUnit> _units;
        private Fraction _fraction;
        private Tween _displayMoving;

        private const int TowerLayer = 7;
        private const int GateLayer = 8;

        public int Power => _power;
        public Fraction Fraction => _fraction;

        public event Action<PlayerSquade> Destroyed; 

        private void OnEnable()
        {
            _spawner.UnitSpawned += OnUnitSpawned;
        }

        private void OnDisable()
        {
            _spawner.UnitSpawned -= OnUnitSpawned;
        }

        private void FixedUpdate()
        {
            transform.Translate(transform.forward * _speed * Time.fixedDeltaTime, Space.World);

            var units = new List<PlayerUnit>(_units);

            foreach (var unit in units)
            {
                unit.CheckDistance();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            switch (other.gameObject.layer)
            {
                case TowerLayer:
                    if (other.TryGetComponent(out Tower tower) && tower == _target)
                    {
                        Attack(tower);
                    }
                    break;
                case GateLayer:
                    if (other.TryGetComponent(out Gate gate))
                    {
                        Resize(gate);
                    }
                    break;
            }
        }

        public void Init(Tower owner, Tower target)
        {
            _isMoving = true;
            _target = target;
            _power = owner.Power;
            _fraction = owner.Fraction;
            _display.text = _power.ToString();

            _units = new List<PlayerUnit>();
            _spawner.Spawn(_target.transform.position, _power);

            transform.LookAt(target.transform);
        }

        private void Attack(Tower castle)
        {
            _isMoving = false;
            _displayMoving?.Kill();

            var displayPosition = _display.transform.parent.position;
            _display.transform.parent.SetParent(null);
            _display.transform.parent.position = displayPosition;
            _display.transform.parent.DOMove(castle.PowerPosition, 0.45f).onComplete += () =>
            {
                castle.ApplyAttack(this);
                Destroy(_display.transform.parent.gameObject);
            };
        }

        private void Resize(Gate gate)
        {
            foreach (var unit in _units)
            {
                unit.Hide();
                unit.TargetReached -= OnUnitTargetReached;
            }

            _power = gate.Calculate(_power);
            _display.text = _power.ToString();
            _units.Clear();
            _spawner.Spawn(_target.transform.position, _power);
        }

        private void OnUnitSpawned(PlayerUnit unit)
        {
            unit.TargetReached += OnUnitTargetReached;
            unit.Died += OnUnitDied;

            _units.Add(unit);

            if (_isMoving)
            {
                var position = _display.transform.parent.localPosition;
                var z = unit.transform.localPosition.z / 2f;

                _displayMoving?.Kill();
                _displayMoving = _display.transform.parent.DOLocalMoveZ(z, 0.5f);
            }
        }

        private void OnUnitDied(PlayerUnit unit)
        {
            unit.Died -= OnUnitDied;

            _power -= 3;

            if (_display)
            {
                _display.text = _power.ToString();
            }
        }

        private void OnUnitTargetReached(PlayerUnit unit)
        {
            unit.TargetReached -= OnUnitTargetReached;

            _units.Remove(unit);

            if (_units.Count == 0)
            {
                Destroyed?.Invoke(this);
                Destroy(gameObject);
            }
        }
    }
}