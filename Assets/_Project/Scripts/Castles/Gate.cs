﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Castles
{
    [RequireComponent(typeof(BoxCollider), typeof(Rigidbody))]
    public class Gate : MonoBehaviour
    {
        [SerializeField] private GateType _type;
        [SerializeField] private int _value;
        [SerializeField] private Text _text;
        [SerializeField] private GameObject _gate;
        [SerializeField] private ParticleSystem _breakEffect;

        public event Action<int> Broken;
        
        private void Start()
        {
            switch (_type)
            {
                case GateType.Add:
                    _text.text = "+" + _value;
                    break;
                case GateType.Multiply:
                    _text.text = "x" + _value;
                    break;
                default:
                    throw new ArgumentException("Non implemented gate type");
            }
        }
        
        public int Calculate(int value)
        {
            GetComponent<Collider>().enabled = false;

            _text.enabled = false;
            _gate.SetActive(false);
            _breakEffect.Play();

            int calculated;

            switch (_type)
            {
                case GateType.Add:
                    calculated = value + _value;
                    break;
                case GateType.Multiply:
                    calculated = value * _value;
                    break;
                default:
                    throw new ArgumentException("Non implemented gate type");
            }

            Broken?.Invoke(calculated);

            return calculated;
        }
    }
}