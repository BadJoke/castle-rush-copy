using Castles.Squades;
using System;
using UnityEngine;

namespace Castles
{
    [RequireComponent(typeof(TowerAppearence), typeof(TowerPower))]
    public class Tower : MonoBehaviour
    {
        [SerializeField] private Fraction _fraction;
        [SerializeField] private Tower[] _targets; 

        private PlayerSquade _squadePrefab;
        private TowerAppearence _appearence;
        private FlagPresenter _flagPresenter;
        private TowerPower _power;

        private const string SquadePrefabPath = "Prefabs/Units/Squade";

        public event Action<int> UnitsTaked;

        public int Power => _power.Amount;
        public Fraction Fraction => _fraction;
        public Vector3 PowerPosition => _power.Position;

        private void Awake()
        {
            _appearence = GetComponent<TowerAppearence>();
            _appearence.UpdateAppearence(_fraction);
            _flagPresenter = GetComponent<FlagPresenter>();
            _power = GetComponent<TowerPower>();
            _squadePrefab = Resources.Load<PlayerSquade>(SquadePrefabPath);
        }

        public void StartAttack(Tower target)
        {
            Instantiate(_squadePrefab, transform).Init(this, target);

            _fraction = Fraction.Empty;
            _power.SetPower(0);
            _flagPresenter?.Show();
        }

        public void ApplyAttack(PlayerSquade attacker)
        {
            if (_power.Amount == 0)
            {
                _flagPresenter?.Hide();
            }

            if (attacker.Power < _power.Amount)
            {
                _power.ApplyAttack(-attacker.Power);

                UnitsTaked?.Invoke(0);
            }
            else
            {
                _power.ApplyAttack(attacker.Power);
                _fraction = attacker.Fraction;
                _appearence.UpdateAppearence(_fraction);

                UnitsTaked?.Invoke(_power.Amount);
            }
        }

        public bool HasTarget(Tower tower)
        {
            foreach (var target in _targets)
            {
                if (target == tower)
                {
                    return true;
                }
            }

            return false;
        }

        public void NullifyPower(float time)
        {
            _power.Nullify(time, _flagPresenter.Show);
        }
    }
}