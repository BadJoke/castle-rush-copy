﻿using DG.Tweening;
using UnityEngine;

namespace Castles
{
    public class FlagPresenter : MonoBehaviour
    {
        [SerializeField] private Transform _flagpole;
        [SerializeField] private float _flagPoleHeight = 4.5f;
        [SerializeField] private float _flagPoleYAngle = 180f;

        private void Start()
        {
            var eulers = _flagpole.rotation.eulerAngles;
            eulers.y = _flagPoleYAngle;
            _flagpole.rotation = Quaternion.Euler(eulers);
        }

        public void Show()
        {
            _flagpole.DOLocalMoveY(_flagPoleHeight, 0.5f);
        }

        public void Hide()
        {
            _flagpole.DOLocalMoveY(3f, 0.5f);
        }
    }
}