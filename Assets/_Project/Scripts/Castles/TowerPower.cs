﻿using DG.Tweening;
using DG.Tweening.Core;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Castles
{
    public class TowerPower : MonoBehaviour
    {
        [SerializeField] private int _amount;
        [SerializeField] private Text _text;
        [SerializeField] private Transform _puff;
        [SerializeField] private float _puffSize = 25f;
        [SerializeField] private float _puffTime = 0.2f;

        public int Amount => _amount;
        public Vector3 Position => _text.transform.position;

        private void Start()
        {
            _text.text = _amount.ToString();
        }

        public void SetPower(int amount)
        {
            _amount = amount;
            _text.text = _amount == 0 ? "" : _amount.ToString();
        }

        public void ApplyAttack(int influence)
        {
            _amount += influence;

            var puffIncreasing = _puff.DOScale(_puffSize, _puffTime).Pause();
            var puffDecreasing = _puff.DOScale(0, _puffTime).Pause();

            puffIncreasing.onComplete += () =>
            {
                _text.text = _amount.ToString();
            };

            DOTween.Sequence()
                .Append(puffIncreasing)
                .Append(puffDecreasing);
        }

        public void Nullify(float time, Action completeAction)
        {
            DOGetter<int> getter = () => _amount;
            DOSetter<int> setter = (int value) =>
            {
                _amount = value;
                _text.text = _amount.ToString();
            };

            var tween = DOTween.To(getter, setter, 0, time);
            tween.onComplete += new TweenCallback(completeAction);
            tween.onComplete += () =>
                {
                    _text.enabled = false;
                };
        }
    }
}