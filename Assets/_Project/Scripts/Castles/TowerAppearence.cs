﻿using UnityEngine;

namespace Castles
{
    public class TowerAppearence : MonoBehaviour
    {
        [SerializeField] private TowerChanger _base;
        [SerializeField] private TowerChanger _magic;
        [SerializeField] private TowerType _type;

        public void UpdateAppearence(Fraction fraction)
        {
            switch (_type)
            {
                case TowerType.Base:
                    _base.SetTower(fraction);
                    _magic.Deacticate();
                    break;
                case TowerType.Magic:
                    _magic.SetTower(fraction);
                    _base.Deacticate();
                    break;
                default:
                    throw new System.ArgumentException("Incorrect type\n" +
                        "Must be Base or Magic");
            }
        }
    }
}