﻿using UnityEngine;

namespace Castles
{
    public class TowerChanger : MonoBehaviour
    {
        [SerializeField] private GameObject _player;
        [SerializeField] private GameObject _enemy;

        public void SetTower(Fraction fraction)
        {
            switch (fraction)
            {
                case Fraction.Player:
                    _player.SetActive(true);
                    _enemy.SetActive(false);
                    break;
                case Fraction.Enemy:
                    _player.SetActive(false);
                    _enemy.SetActive(true);
                    break;
                default:
                    throw new System.ArgumentException("Incorrect fraction\n" +
                         "Must be Player or Enemy");
            }
        }

        public void Deacticate()
        {
            gameObject.SetActive(false);
        }
    }
}