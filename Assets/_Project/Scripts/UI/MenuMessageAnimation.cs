﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class MenuMessageAnimation : MonoBehaviour
    {
        [SerializeField] private float _interval;
        [SerializeField] private float _angle = 15f;
        [SerializeField] private float _size = 1.2f;

        private void Start()
        {
            var text = GetComponent<Text>();
            var outline = GetComponent<Outline>();

            /*
            DOTween.Sequence()
                .Append(text.DOFade(40f / 256f, _interval / 2f))
                .Join(outline.DOFade(40f / 256f, _interval / 2f))
                .Append(text.DOFade(1f, _interval / 2f))
                .Join(outline.DOFade(1f, _interval / 2f))
                .SetLoops(-1);
            */

            var duration = _interval / 4f;

            DOTween.Sequence()
                .Append(transform.DOLocalRotate(new Vector3(0f, 0f, _angle), duration))
                .Join(transform.DOScale(_size, duration))
                .Append(transform.DOLocalRotate(Vector3.zero, duration))
                .Join(transform.DOScale(1f, duration))
                .Append(transform.DOLocalRotate(new Vector3(0f, 0f, -_angle), duration))
                .Join(transform.DOScale(_size, duration))
                .Append(transform.DOLocalRotate(Vector3.zero, duration))
                .Join(transform.DOScale(1f, duration))
                .SetLoops(-1);
        }
    }
}