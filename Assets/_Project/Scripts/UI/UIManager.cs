﻿using General;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private Text _levelNumber;
        [SerializeField] private Menu _menu;
        [SerializeField] private ProgressController _progressController;
        [SerializeField] private FinalBattle _finalBattle;
        [SerializeField] private ProgressBar _progressBar;
        [SerializeField] private RestartButton _restartButton;
        [SerializeField] private FinalPanel _finalPanel;

        private bool _isRestarting;
        private string _levelID;
        private LevelFinishedResult _result;
        private int _playtime;
        private Stopwatch _stopWatch;
        private LevelManager _levelManager;

        private void Awake()
        {
            _isRestarting = true;
            _stopWatch = new Stopwatch();
            _levelManager = LevelManager.Instance;
            _levelNumber.text = "LEVEL " + _levelManager.CurrentLevel.ToString();

            _menu.GameStrted += OnGameStarted;
            _finalBattle.CastleCaptured += OnCastleCaptured;
            _progressController.Wone += OnWone;
            _progressController.Losed += OnLosed;
        }

        public void OnClick()
        {
            if (_isRestarting)
            {
                _stopWatch.Stop();

                _result = LevelFinishedResult.manual_restart;
                _playtime = _stopWatch.Elapsed.Seconds;

                HoopslyIntegration.Instance.RaiseLevelFinishedEvent(_levelID, _result, _playtime);
            }

            _levelManager.LoadLevel();
        }

        private void UnlistenProgress()
        {
            _progressController.Wone -= OnWone;
            _progressController.Losed -= OnLosed;
        }

        private void HideUI()
        {
            _progressBar.Hide();
            _restartButton.Hide();
        }

        private string DetermineLevelID()
        {
            var level = _levelManager.CurrentLevel;
            string id = "level_";

            if (level < 10)
            {
                id += "0" + level;
            }
            else
            {
                id += level.ToString();
            }

            return id;
        }

        private void SendEvent(bool hasWin)
        {
            _isRestarting = false;
            _stopWatch.Stop();

            _result = hasWin ? LevelFinishedResult.win : LevelFinishedResult.lose;
            _playtime = _stopWatch.Elapsed.Seconds;

            HoopslyIntegration.Instance.RaiseLevelFinishedEvent(_levelID, _result, _playtime);
        }

        private void OnGameStarted()
        {
            _restartButton.Show();
            _progressBar.Show();
            _stopWatch.Start();

            _levelID = DetermineLevelID();
            HoopslyIntegration.Instance.RaiseLevelStartEvent(_levelID);
        }

        private void OnWone(float delay)
        {
            LevelManager.Instance.SaveProgress();
            SendEvent(true);
            UnlistenProgress();
            Invoke(nameof(HideUI), delay);
        }

        private void OnLosed()
        {
            SendEvent(false);
            UnlistenProgress();
            HideUI();
            _finalPanel.Show(false);
        }

        private void OnCastleCaptured()
        {
            _finalBattle.CastleCaptured -= OnCastleCaptured;
            _finalPanel.Show(true);
        }
    }
}