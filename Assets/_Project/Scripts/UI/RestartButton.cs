using DG.Tweening;
using UnityEngine;

namespace UI
{
    public class RestartButton : MonoBehaviour
    {
        [SerializeField] private float _showOffset = 33f;

        private RectTransform _rectTransform;

        private void Awake()
        {
            _rectTransform = transform as RectTransform;
        }

        public void Show()
        {
            _rectTransform.DOAnchorPosX(_showOffset, 0.5f);
        }

        public void Hide()
        {
            _rectTransform.DOAnchorPosX(-_showOffset, 0.5f);
        }
    }
}