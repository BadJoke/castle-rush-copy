﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class FinalPanel : MonoBehaviour
    {
        [SerializeField] private float _messagePosition;
        [SerializeField, Range(0f, 256f)] private float _fading = 40f;
        [SerializeField] private Image _bakground;
        [SerializeField] private Text _message;
        [SerializeField] private RectTransform _messsageTransform;
        [SerializeField] private Text _button;
        [SerializeField] private Transform _buttonTransform;

        private const string WinMessage = "COMPLETED!";
        private const string LoseMessage = "FAILED...";
        private const string WinButton = "NEXT";
        private const string LoseButton = "RESTART";

        public void Show(bool hasWin)
        {
            SetValues(hasWin);

            _messsageTransform.DOAnchorPosY(_messagePosition, 0.5f);
            DOTween.Sequence()
                .Join(_buttonTransform.DOScale(1.2f, 0.4f))
                .Append(_messsageTransform.DOScale(1f, 0.1f));
        }

        private void SetValues(bool hasWin)
        {
            _message.text = hasWin ? WinMessage : LoseMessage;
            _button.text = hasWin ? WinButton : LoseButton;

            if (hasWin == false)
            {
                _bakground.DOFade(_fading / 256f, 0.1f);
            }
        }
    }
}