﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public class Menu : MonoBehaviour, IPointerDownHandler
    {
        public event Action GameStrted;

        public void OnPointerDown(PointerEventData eventData)
        {
            gameObject.SetActive(false);
            GameStrted?.Invoke();
        }
    }
}