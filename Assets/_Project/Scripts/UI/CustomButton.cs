﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class CustomButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
    {
        [SerializeField] private Sprite _clickedSprite;
        [SerializeField] private float _pressingDepth = 12f;
        [SerializeField] private UnityEvent _clicked;

        private Image _buttonImage;
        private Sprite _defaultSprite;
        private RectTransform _selfRTransform;

        private void Start()
        {
            _selfRTransform = GetComponent<RectTransform>();
            _buttonImage = GetComponent<Image>();
            _defaultSprite = _buttonImage.sprite;
        }

        public void OnPointerClick(PointerEventData pointerData)
        {
            _clicked?.Invoke();
        }

        public void OnPointerDown(PointerEventData pointerData)
        {
            _selfRTransform.Translate(Vector2.down * _pressingDepth);

            if (_clickedSprite != null)
                _buttonImage.sprite = _clickedSprite;
        }

        public void OnPointerUp(PointerEventData pointerData)
        {
            _selfRTransform.Translate(Vector2.up * _pressingDepth);

            if (_clickedSprite != null)
                _buttonImage.sprite = _defaultSprite;
        }
    }
}
