﻿using Castles;
using DG.Tweening;
using General;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField] private float _yPosition;
        [SerializeField] private float _fillingSpeed = 0.2f;
        [SerializeField] private Image[] _fractions;
        [SerializeField] private ProgressController _controller;

        private RectTransform _rect;

        private void OnEnable()
        {
            _controller.UnitCountChanged += OnUnitCountChanged;
        }

        private void Awake()
        {
            _rect = transform as RectTransform;
        }

        private void OnDisable()
        {
            _controller.UnitCountChanged -= OnUnitCountChanged;
        }

        public void Show()
        {
            _rect.DOAnchorPosY(_yPosition, 0.25f);
        }

        public void Hide()
        {
            _rect.DOAnchorPosY(-_yPosition, 0.25f);
        }

        private void OnUnitCountChanged(int totalPower, Dictionary<Fraction, int> fractionPowers)
        {
            var lastPower = fractionPowers[Fraction.Player];

            _fractions[0].DOFillAmount((float)lastPower / totalPower, _fillingSpeed);

            for (int i = 1; i < _fractions.Length; i++)
            {
                if (fractionPowers.TryGetValue((Fraction)i, out int power))
                {
                    lastPower += power;
                    _fractions[i].DOFillAmount((float)lastPower / totalPower, _fillingSpeed);
                }
            }
        }
    }
}