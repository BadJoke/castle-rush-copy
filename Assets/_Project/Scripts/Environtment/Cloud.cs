﻿using UnityEngine;

namespace Environtment
{
    public class Cloud : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _finalPosition;

        private float _replacePosition;

        private void Update()
        {
            transform.Translate(Vector3.back * _speed * Time.deltaTime, Space.World);

            if (transform.position.z <= _finalPosition)
            {
                var position = transform.position;
                position.z = _replacePosition;
                transform.position = position;
            }
        }

        public void Init(float position)
        {
            _replacePosition = position;

            transform.localScale = Vector3.one * Random.Range(0.3f, 0.7f);
        }
    }
}
