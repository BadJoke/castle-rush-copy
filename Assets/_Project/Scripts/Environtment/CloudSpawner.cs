﻿using UnityEngine;

namespace Environtment
{
    public class CloudSpawner : MonoBehaviour
    {
        [SerializeField] private float _offset;
        [SerializeField] private float _offsetSpread;
        [SerializeField] private float _height;
        [SerializeField] private float _heightSpread;
        [SerializeField] private float _length;
        [SerializeField] private int _cloudCounts;
        [SerializeField] private Cloud _cloudPrefab;

        private int _counter;
        private float _previousOffset;

        private void Awake()
        {
            SpawnClouds();
        }

        private void OnDrawGizmos()
        {
            var position = transform.position;
            position.y += _height;
            position.z += _length / 2f;

            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(position, new Vector3(_offset + (_offsetSpread / 2f), _heightSpread, _length));

            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(position, new Vector3(_offset - (_offsetSpread / 2f), _heightSpread, _length));
        }

        private void SpawnClouds()
        {
            _counter = 0;
            var step = _length / _cloudCounts;
            var position = transform.position;
            position.y += _height;

            for (int i = 0; i < _cloudCounts; i++, position.z += step)
            {
                SpawnCloud(position);
            }
        }

        private void SpawnCloud(Vector3 position)
        {
            var offset = Random.value > 0.5f ? -_offset / 2f : _offset / 2f;

            if (offset == _previousOffset)
            {
                _counter++;

                if (_counter > 2)
                {
                    offset = -offset;
                    _counter = 0;
                }
            }

            _previousOffset = offset;

            position.x += offset;
            position.x += Random.Range(-_offsetSpread / 2f, _offsetSpread / 2f);
            position.y += Random.Range(-_heightSpread / 2f, _heightSpread / 2f);
            position.z += Random.Range(-_heightSpread / 2f, _heightSpread / 2f);

            var rotation = Quaternion.Euler(new Vector3(0f, Random.Range(0f, 360f), 0f));

            var cloud = Instantiate(_cloudPrefab, position, rotation, transform);
            cloud.Init(_length);
        }
    }
}
