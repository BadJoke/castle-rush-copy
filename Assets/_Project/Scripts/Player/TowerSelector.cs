﻿using Castles;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(SelectionLine))]
    public class TowerSelector : MonoBehaviour
    {
        [SerializeField] private LayerMask _groundMask;
        [SerializeField] private LayerMask _towerMask;
        [SerializeField] private PlayerMover _playerMover;

        private float _raycastDistance = 100f;
        private Camera _camera;
        private SelectionLine _line;
        private Tower _playerTower;
        private Tower _targetTower;

        private void Awake()
        {
            _camera = Camera.main;
            _line = GetComponent<SelectionLine>();
        }

        public bool StartSelecting()
        {
            if (TryFindPlayer())
            {
                _line.SetStart(_playerTower.transform.position);

                return true;
            }

            return false;
        }

        public void Selecting()
        {
            if (TryFindTarget())
            {
                _line.SetPosition(_targetTower.transform.position);
            }
            else
            {
                _line.SetPosition(GetWorldPosition());
            }
        }

        public void Stop()
        {
            if (TryFindTarget() && _playerTower)
            {
                _playerTower.StartAttack(_targetTower);
                _playerMover.Move(_targetTower.transform.position.z);
            }

            _line.SetDefault();

            _playerTower = null;
            _targetTower = null;
        }

        private bool TryFindTarget()
        {
            return TryFindTower(out _targetTower) && _playerTower.HasTarget(_targetTower);
        }

        private bool TryFindPlayer()
        {
            return TryFindTower(out _playerTower) && _playerTower.Fraction == Fraction.Player;
        }

        private bool TryFindTower(out Tower tower)
        {
            var ray = _camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit, _raycastDistance, _towerMask))
            {
                tower = hit.transform.GetComponent<Tower>();
            }
            else
            {
                tower = null;
            }

            return tower;
        }

        private Vector3 GetWorldPosition()
        {
            var ray = _camera.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out RaycastHit hit, _raycastDistance, _groundMask);
            return hit.point;
        }
    }
}