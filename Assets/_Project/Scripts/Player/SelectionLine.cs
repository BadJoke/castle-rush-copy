﻿using UnityEngine;

namespace Player
{
    public class SelectionLine : MonoBehaviour
    {
        [SerializeField] private float _arrowsSpeed = 3f;
        [SerializeField] private Material _arrowsMaterial;
        [SerializeField] private LineRenderer _line;
        [SerializeField] private SpriteRenderer _start;
        [SerializeField] private SpriteRenderer _end;

        private Vector3 _lineOffset = new Vector3(0f, 0.05f, 0f);
        private Vector2 _arrowTextureOffset;
        private Vector3[] _zeroSelection =
        {
            Vector3.zero,
            Vector3.zero
        };

        private const string ArrowTexture = "_MainTex";

        private void Awake()
        {
            _arrowTextureOffset = _arrowsMaterial.GetTextureOffset(ArrowTexture);
        }

        private void Update()
        {
            _arrowTextureOffset.x += -_arrowsSpeed * Time.deltaTime;
            _arrowsMaterial.SetTextureOffset(ArrowTexture, _arrowTextureOffset);
        }

        private void OnDestroy()
        {
            _arrowsMaterial.SetTextureOffset(ArrowTexture, Vector2.zero);
        }

        public void SetStart(Vector3 position)
        {
            var startPosition = position + _lineOffset;

            _line.SetPosition(0, startPosition);
            _start.transform.position = startPosition;
            _end.transform.position = startPosition;

            _start.enabled = true;
            _end.enabled = true;
        }

        public void SetPosition(Vector3 position)
        {
            position += _lineOffset;

            _line.SetPosition(1, position);
            _end.transform.position = position;
        }

        public void SetDefault()
        {
            _start.enabled = false;
            _end.enabled = false;
            _line.SetPositions(_zeroSelection);
        }
    }
}