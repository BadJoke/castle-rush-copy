﻿using DG.Tweening;
using UnityEngine;

namespace Player
{
    public class PlayerMover : MonoBehaviour
    {
        [SerializeField] private float _offset = -15.24427f;
        [SerializeField] private float _speed = 12f;

        private Tween _moving;

        public void Move(float position)
        {
            if (_moving != null)
            {
                _moving.Kill();
            }

            var time = (position - transform.position.z) / _speed;
            _moving = transform.DOMoveZ(position + _offset, time);
        }
    }
}