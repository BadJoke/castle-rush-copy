using Castles;
using UnityEngine;

namespace Player
{
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] private TowerSelector _selector;

        private bool _isPLayerSelected = false;

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _isPLayerSelected = _selector.StartSelecting();
            }

            if (Input.GetMouseButton(0) && _isPLayerSelected)
            {
                _selector.Selecting();
            }

            if (Input.GetMouseButtonUp(0) && _isPLayerSelected)
            {
                _selector.Stop();
                _isPLayerSelected = false;
            }
        }
    }
}