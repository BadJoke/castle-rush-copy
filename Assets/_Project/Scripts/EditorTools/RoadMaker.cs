﻿using UnityEngine;

namespace EditorTools
{
    public class RoadMaker : MonoBehaviour
    {
        [SerializeField] private Transform _from;
        [SerializeField] private Transform[] _to;
        [SerializeField] private Transform _container;
        [SerializeField] private LineRenderer _roadPrefab;

        private Vector3 _offest = new Vector3(0f, 0.01f, 0f);

        public void Create()
        {
            foreach (var target in _to)
            {
                Instantiate(_roadPrefab, _container)
                    .SetPositions(new Vector3[]
                    {
                        _from.position + _offest,
                        target.position + _offest
                    });
            }
        }
    }
}
