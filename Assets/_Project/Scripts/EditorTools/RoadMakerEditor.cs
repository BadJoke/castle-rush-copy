﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace EditorTools
{
    [CustomEditor(typeof(RoadMaker))]
    public class RoadMakerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var roadMaker = target as RoadMaker;

            if (GUILayout.Button("Create"))
            {
                roadMaker.Create();
            }
        }
    }
}
#endif