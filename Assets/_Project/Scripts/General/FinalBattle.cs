﻿using Castles;
using Castles.Squades;
using System;
using UnityEngine;

namespace General
{
    public class FinalBattle : MonoBehaviour
    {
        [SerializeField] private int _bossPower;
        [SerializeField] private float _battleDelay = 0.25f;
        [SerializeField] private Transform _castlePoint;
        [SerializeField] private FinalSquade _playerSquade;
        [SerializeField] private FinalSquade _bossSquade;
        [SerializeField] private ProgressController _controller;
        [SerializeField] private CastleRenderer _recolorer;
        [SerializeField] private ParticleSystem[] _confetti;

        private bool _isWone = false;
        private Tower _playerTower;

        public event Action CastleCaptured;

        private void Awake()
        {
            _controller.Wone += OnWone;
        }

        private void ShowUnits()
        {
            _playerTower = _controller.GetPlayerTower();

            var playerSquadePosition = _playerTower.transform.position;
            playerSquadePosition.x = 0f;

            _playerSquade.transform.position = playerSquadePosition;
            _playerSquade.Init(_playerTower.Power, _playerTower.transform.position);
            _bossSquade.Init(_bossPower, transform.position);

            _playerTower.NullifyPower(_playerSquade.Spawner.GetTime());

            _playerSquade.Spawner.Spawned += OnPlayerSquadeSpawned;
            _playerSquade.Destroyed += OnPlayerSquadeDestroyed;
            _bossSquade.Destroyed += OnBossSquadeDestroyed;
        }

        private void StartBattle()
        {
            _playerSquade.StartMovement(_bossSquade);
            _bossSquade.StartMovement(_playerSquade);
        }

        private void Win()
        {
            _recolorer.SetWin();

            foreach (var confetti in _confetti)
            {
                confetti.Play();
            }

            CastleCaptured?.Invoke();
        }

        private void OnWone(float delay)
        {
            _controller.Wone -= OnWone;

            Invoke(nameof(ShowUnits), delay);
        }

        private void OnPlayerSquadeSpawned()
        {
            _playerSquade.Spawner.Spawned -= OnPlayerSquadeSpawned;

            Invoke(nameof(StartBattle), _battleDelay);
        }

        private void OnBossSquadeDestroyed()
        {
            _bossSquade.Destroyed -= OnBossSquadeDestroyed;

            _isWone = true;
            _playerSquade.MoveToCastle(_castlePoint);
        }

        private void OnPlayerSquadeDestroyed()
        {
            _playerSquade.Destroyed -= OnPlayerSquadeDestroyed;

            if (_isWone)
            {
                Win();
            }
        }
    }
}