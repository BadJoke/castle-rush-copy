﻿using Castles;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace General
{
    public class ProgressController : MonoBehaviour
    {
        [SerializeField] private float _woneActionsDelay = 0.5f;

        private Gate[] _gates;
        private Tower[] _towers;

        public event Action<int, Dictionary<Fraction, int>> UnitCountChanged;
        public event Action<float> Wone;
        public event Action Losed;

        private void Awake()
        {
            _gates = FindObjectsOfType<Gate>();
            _towers = FindObjectsOfType<Tower>();
        }

        private void Start()
        {
            foreach (var gate in _gates)
            {
                gate.Broken += OnGateBroken;
            }

            foreach (var tower in _towers)
            {
                if (tower.Fraction == Fraction.Player)
                {
                    RecalculateProgress(tower.Power);
                }

                tower.UnitsTaked += OnUnitsTaked;
            }
        }

        private void OnDestroy()
        {
            foreach (var gate in _gates)
            {
                gate.Broken -= OnGateBroken;
            }

            foreach (var castle in _towers)
            {
                castle.UnitsTaked -= OnUnitsTaked;
            }
        }

        public Tower GetPlayerTower()
        {
            foreach (var tower in _towers)
            {
                if (tower.Fraction == Fraction.Player)
                {
                    return tower;
                }
            }

            return null;
        }

        private void RecalculateProgress(int playerPower)
        {
            if (playerPower == 0)
            {
                Losed?.Invoke();
            }

            var totalPower = playerPower;
            var fractionPowers = new Dictionary<Fraction, int>();
            fractionPowers.Add(Fraction.Player, playerPower);

            foreach (var castle in _towers)
            {
                if (castle.Fraction <= Fraction.Player)
                {
                    continue;
                }

                if (fractionPowers.TryGetValue(castle.Fraction, out int power))
                {
                    power += castle.Power;

                    fractionPowers.Remove(castle.Fraction);
                    fractionPowers.Add(castle.Fraction, power);
                }
                else
                {
                    fractionPowers.Add(castle.Fraction, castle.Power);
                }

                totalPower += castle.Power;
            }

            if (totalPower == playerPower)
            {
                Wone?.Invoke(_woneActionsDelay);
            }

            UnitCountChanged?.Invoke(totalPower, fractionPowers);
        }

        private void OnUnitsTaked(int power)
        {
            RecalculateProgress(power);
        }

        private void OnGateBroken(int power)
        {
            RecalculateProgress(power);
        }
    }
}
