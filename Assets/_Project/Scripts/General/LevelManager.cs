﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace General
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] private int _level = 0;

        private int _currentLevel = 1;
        private int _currentTestLevel = 0;

        public static LevelManager Instance;

        private const string LevelKey = "CurrentLevel";

        public int CurrentLevel => IsTestLaunch ? _currentTestLevel : _currentLevel;
        private bool IsTestLaunch => _level > 0;

        private void Awake()
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            if (IsTestLaunch)
            {
                _currentTestLevel = _level;
            }
            else
            {
                _currentLevel = PlayerPrefs.HasKey(LevelKey) ? PlayerPrefs.GetInt(LevelKey) : 1;
            }

            LoadLevel();
        }

        public void LoadLevel()
        {
            if (_level < 0)
            {
                PlayerPrefs.SetInt(LevelKey, 1);
            }

            SceneManager.LoadScene(SelectLevel(CurrentLevel));
        }

        public void SaveProgress()
        {
            if (IsTestLaunch)
            {
                _currentTestLevel++;
            }
            else
            {
                PlayerPrefs.SetInt(LevelKey, ++_currentLevel);
            }
        }

        private int SelectLevel(int number)
        {
            int gameScenesCount = SceneManager.sceneCountInBuildSettings - 1;

            if (number > gameScenesCount)
            {
                Random.InitState(number - gameScenesCount);
                return Random.Range(1, gameScenesCount + 1);
            }
            else
            {
                return number;
            }
        }
    }
}