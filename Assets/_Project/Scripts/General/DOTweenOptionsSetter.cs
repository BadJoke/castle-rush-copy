﻿using DG.Tweening;
using UnityEngine;

namespace General
{
    public class DOTweenOptionsSetter : MonoBehaviour
    {
        private void Awake()
        {
            DOTween.SetTweensCapacity(1000, 500);
        }
    }
}
