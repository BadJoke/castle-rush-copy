﻿using DG.Tweening;
using UnityEngine;

namespace General
{
    public class CastleRenderer : MonoBehaviour
    {
        [SerializeField] private float _flagsShowTime = 0.25f;
        [SerializeField] private Transform _flags;
        [SerializeField] private Material _material;
        [SerializeField] private MeshRenderer _renderer;

        private const int _index = 1;

        public void SetWin()
        {
            _flags.DOScale(1f, _flagsShowTime);

            Recolor();
        }

        private void Recolor()
        {
            var materials = _renderer.materials;

            materials[_index] = _material;

            _renderer.materials = materials;
        }
    }
}
