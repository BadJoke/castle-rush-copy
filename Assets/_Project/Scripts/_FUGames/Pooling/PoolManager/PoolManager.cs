﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FUGames.Pooling
{
    public class PoolManager : MonoBehaviour
    {
        [SerializeField] private PoolParams[] _paramsArray;

        private Dictionary<Type, Pool> _pools;

        private static PoolManager _instance;

        public static PoolManager Instance => _instance;

        private void Awake()
        {
            _instance = this;
            _pools = new Dictionary<Type, Pool>();

            foreach (var param in _paramsArray)
            {
                var type = param.Template.GetType();

                _pools.Add(type, new Pool(param.Template, param.StartSize, this));
            }
        }

        public T Take<T>() where T : PoolObject
        {
            var type = typeof(T);

            if (_pools.ContainsKey(type))
            {
                return _pools[type].TakeObject<T>();
            }

            throw new ArgumentException("Pool manager does not contain this type of object");
        }

        public void Put(PoolObject poolable)
        {
            var type = poolable.GetType();

            if (_pools.ContainsKey(type))
            {
                _pools[type].PutObject(poolable);
            }
        }
    }
}
