﻿using System.Collections.Generic;
using UnityEngine;

namespace FUGames.Pooling
{
    public class Pool
    {
        private Queue<PoolObject> _pool;
        private Transform _parent;
        private PoolObject _template;
        private PoolManager _manager;

        public Pool(PoolObject template, int size, PoolManager manager)
        {
            _pool = new Queue<PoolObject>();
            _manager = manager;
            _parent = manager.transform;
            _template = template;

            for (int i = 0; i < size; i++)
            {
                Create();
            }
        }

        public T TakeObject<T>() where T : PoolObject
        {
            if (_pool.Count == 0)
            {
                Create();
            }

            return (T)_pool.Dequeue();
        }

        public void PutObject(PoolObject poolable)
        {
            var transform = poolable.transform;
            transform.SetParent(_parent);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(Vector3.zero);

            poolable.gameObject.SetActive(false);

            _pool.Enqueue(poolable);
        }

        private void Create()
        {
            var poolObject = Object.Instantiate(
                    _template,
                    _parent.position,
                    Quaternion.identity,
                    _parent);

            poolObject.SetManager(_manager);
            poolObject.gameObject.SetActive(false);

            _pool.Enqueue(poolObject);
        }
    }
}